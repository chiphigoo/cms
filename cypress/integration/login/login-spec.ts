import { And, Given, Then, When } from "cypress-cucumber-preprocessor/steps";

const url = `https://cms.detailonline.com/`;

//user will login with correct credentials
Given(`user is in main page`, () => {
  cy.visit(url, { timeout: 40000 });
});

When(`user types on the right email field`, () => {
  cy.get(
    ":nth-child(1) > .el-form-item__content > .el-input > .el-input__inner"
  ).type("chin.lu@detailonline.tech");
});

And(`user types on the right password field`, () => {
  cy.get(
    ":nth-child(2) > .el-form-item__content > .el-input > .el-input__inner"
  ).type("chinezlove22{enter}");
});

Then(`user is back to the home page but with the account logged in`, () => {
  cy.wait(10000);
  cy.contains("Chinphiline Lu");
});

//user will login with wrong email
Given(`user is in main page`, () => {
  cy.visit(url, { timeout: 40000 });
});

When(`user types on the wrong email field`, () => {
  cy.get(
    ":nth-child(1) > .el-form-item__content > .el-input > .el-input__inner"
  ).type("chinlu@detailonline.tech");
});

And(`user types on the right password field`, () => {
  cy.get(
    ":nth-child(2) > .el-form-item__content > .el-input > .el-input__inner"
  ).type("chinezlove22{enter}");
});

Then(`user should see an error message`, () => {
  cy.wait(1000);
  cy.contains("Invalid email or password​");
});

//user will login with wrong password
Given(`user is in main page`, () => {
  cy.visit(url, { timeout: 40000 });
});

When(`user types on the right email field`, () => {
  cy.get(
    ":nth-child(1) > .el-form-item__content > .el-input > .el-input__inner"
  ).type("chin.lu@detailonline.tech");
});

And(`user types on the wrong password field`, () => {
  cy.get(
    ":nth-child(2) > .el-form-item__content > .el-input > .el-input__inner"
  ).type("password{enter}");
});

Cypress.on("uncaught:exception", () => {
  return false;
});
