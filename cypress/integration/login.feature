Feature: User logging in

Scenario: user will login with correct credentials
Given user is in main page
When user types on the right email field
And user types on the right password field
Then user is back to the home page but with the account logged in

Scenario: user will login with wrong email
Given user is in main page
When user types on the wrong email field
And user types on the right password field
Then user should see an error message

Scenario: user will login with wrong password
Given user is in main page
When user types on the right email field
And user types on the wrong password field
Then user should see an error message